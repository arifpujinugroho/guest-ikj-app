// First import createStore function from Framework7 core
import { createStore } from 'framework7/lite';
let linkUrl ="https://infokajian.org";
// create store
const store = createStore({
  // start with the state (store data)
  state: {
    loading: false,
    kajians: [],
    kajiansNextUrl:null,
    doas: [],
    blogs: [],
    blogsNextUrl: null,
    videos: [],
    sholat: null,
    lapkeu: [],
    lapkeuNextUrl: null,
    transaksi: [],
    transaksiNextUrl: null,
  },

  // actions to operate with state and for async manipulations
  actions: {
    addDoas({ state }, doa) {
      state.doas = doa;
    },
    addSholat({ state }, sholat) {
      state.sholat = sholat;
    },
    // context object containing store state will be passed as an argument
    kajianRefreshData({ state }) {
      state.kajians = [];
      state.kajiansNextUrl = null;
      fetch(linkUrl+'/app/kajian/semua')
        .then((res) => res.json())
        .then((kajians) => {
          state.kajians = kajians.data;
          state.kajiansNextUrl = kajians.next_page_url;
        })
    },
    kajianAddingData({ state }) {
      state.loading = true;
      fetch(state.kajiansNextUrl)
        .then((res) => res.json())
        .then((kajians) => {
          state.kajians = state.kajians.concat(kajians.data);
          state.kajiansNextUrl = kajians.next_page_url;
          state.loading = false;
        })
    },
    blogRefreshData({ state }) {
      state.blogs = [];
      state.blogsNextUrl = null;
      fetch(linkUrl+'/app/blog/semua')
        .then((res) => res.json())
        .then((blogs) => {
          state.blogs = blogs.data;
          state.blogsNextUrl = blogs.next_page_url;
        })
    },
    blogAddingData({ state }) {
      state.loading = true;
      fetch(state.blogsNextUrl)
        .then((res) => res.json())
        .then((blogs) => {
          state.blogs = state.blogs.concat(blogs.data);
          state.blogsNextUrl = blogs.next_page_url;
          state.loading = false;
        })
    },
    videoRefreshData({ state }) {
      fetch(linkUrl + '/app/video/semua')
        .then((res) => res.json())
        .then((datas) => {
          state.videos = datas;
        })
    },
    doaRefreshData({ state }) {
      fetch(linkUrl + '/app/doa/semua')
        .then((res) => res.json())
        .then((datas) => {
          localStorage.setItem('doaData', JSON.stringify(datas));
          state.doas = datas;
        })
    },
    // context object containing store state will be passed as an argument
    lapkeuRefreshData({ state }) {
      state.lapkeu = [];
      state.lapkeuNextUrl = null;
      // fetch(linkUrl+'/app/lapkeu/semua')
      fetch('http://127.0.0.1:8000/app/lapkeu/semua')
        .then((res) => res.json())
        .then((lapkeu) => {
          state.lapkeu = lapkeu.data;
          state.lapkeuNextUrl = lapkeu.next_page_url;
        })
    },
    lapkeuAddingData({ state }) {
      state.loading = true;
      fetch(state.lapkeuNextUrl)
        .then((res) => res.json())
        .then((lapkeu) => {
          state.lapkeu = state.lapkeu.concat(lapkeu.data);
          state.lapkeuNextUrl = lapkeu.next_page_url;
          state.loading = false;
        })
    },
    transaksiRefreshData({ state },id) {
      state.transaksi = [];
      state.transaksiNextUrl = null;
      // fetch(linkUrl+'/app/lapkeu/transaksi/'+id+'/list')
      fetch('http://127.0.0.1:8000/app/lapkeu/transaksi/'+id+'/list')
        .then((res) => res.json())
        .then((transaksi) => {
          state.transaksi = transaksi.data;
          state.transaksiNextUrl = transaksi.next_page_url;
        })
    },
    transaksiAddingData({ state }) {
      state.loading = true;
      fetch(state.transaksiNextUrl)
        .then((res) => res.json())
        .then((transaksi) => {
          state.transaksi = state.transaksi.concat(transaksi.data);
          state.transaksiNextUrl = transaksi.next_page_url;
          state.loading = false;
        })
    },
  },

  // getters to retrieve the state
  getters: {
    // context object containing store state will be passed as an argument
    loading({ state }) {
      return state.loading;
    },
    kajians({ state }) {
      return state.kajians;
    },
    kajiansNextUrl({ state }) {
      return state.kajiansNextUrl;
    },
    blogs({ state }) {
      return state.blogs;
    },
    blogsNextUrl({ state }) {
      return state.blogsNextUrl;
    },
    videos({ state }) {
      return state.videos;
    },
    doas({ state }) {
      return state.doas;
    },
    sholats({ state }) {
      return state.sholat;
    },
    lapkeu({ state }) {
      return state.lapkeu;
    },
    lapkeuNextUrl({ state }) {
      return state.lapkeuNextUrl;
    },
    transaksi({ state }) {
      return state.transaksi;
    },
    transaksiNextUrl({ state }) {
      return state.transaksiNextUrl;
    },
  }

})

// export store
export default store;