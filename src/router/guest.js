import HomePage from '../pages/home.svelte';
import MenuPage from '../pages/menu.svelte';
import AboutPage from '../pages/about.svelte';
import VideoPage from '../pages/video.svelte';
import sholatPage from '../pages/sholat.svelte';
import KajianPage from '../pages/agenda/agenda.svelte';
import KajianDetail from '../pages/agenda/agenda-detail.svelte';
import BlogPage from '../pages/berita/berita.svelte';
import BlogDetail from '../pages/berita/blog-detail.svelte';
import DoaPage from '../pages/doa/doa.svelte';
import DoaDetail from '../pages/doa/doa-detail.svelte';
import LapKeuangan from '../pages/lapkeu/route';
import AlQuran from '../pages/alquran/route';
var routes = [
  {
    path: '/',
    animate:true,
    component: HomePage,
  },
  {
    path: '/list-menu/',
    options: {
      transition: 'f7-dive',
    },
    asyncComponent: () => MenuPage,
  },
  {
    path: '/about-us/',
    options: {
      transition: 'f7-flip',
    },
    asyncComponent: () => AboutPage,
  },
  {
    path: '/sholat/',
    asyncComponent: () => sholatPage,
  },
  {
    path: '/kajian/',
    asyncComponent: () => KajianPage,
  },
  {
    path: '/kajian/:kajianId/',
    asyncComponent: () => KajianDetail,
  },
  {
    path: '/blog/',
    asyncComponent: () => BlogPage,
  },
  {
    path: '/blog/:blogId/',
    asyncComponent: () => BlogDetail,
  },
  {
    path: '/doa/',
    asyncComponent: () => DoaPage,
  },
  {
    path: '/doa/:doaId/',
    asyncComponent: () => DoaDetail,
  },
  {
    path: '/video/',
    asyncComponent: () => VideoPage,
  }
];
routes = routes.concat(LapKeuangan,AlQuran);
export default routes;