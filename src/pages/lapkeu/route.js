import LapkeuPage from './listlapkeu.svelte';
import TransaksiPage from './listtransaksi.svelte';
var routes = [
  {
    path: '/lapkeu/',
    component: LapkeuPage,
  },
  // {
  //   path: '/lapkeu/:lapkeuId',
  //   component: LapkeuPage,
  // },
  {
    path: '/lapkeu/:lapkeuId/transaksi',
    component: TransaksiPage,
  },
];
export default routes;